# OCI Service Broker on the Oracle Kubernetes Service
In this workshop we will demonstrate how to consume Oracle autonomous database services from a Kubernetes environment.

We will use the following architecture:
![oci_architecture.png](architecture/oci_architecture.png)

To make it easy to replicate, the Kubernetes cluster and all the supporting compute services will be installed with Terraform. 

The Database services will be installed using the Oracle implementation of the Open Service Broker API.

Note that we used the Kubernetes managed service available on the Oracle Cloud, but it would work with another Cloud provider or an on-premise version.

# Kubernetes Cluster deployment

### Generate user API Key
Generate private/public key pair:
```
mkdir ~/api_key
openssl genrsa -out ~/.oci/oci_api_key.pem 2048
openssl rsa -pubout -in ~/.oci/oci_api_key.pem -out ~/.oci/oci_api_key_public.pem
chmod 600 ~/.oci/oci_api_key.pem
openssl rsa -pubout -outform DER -in ~/.oci/oci_api_key.pem | openssl md5 -c
```

Then upload the key to the OCI console.

### Install and configure the CLI
```
bash -c "$(curl -L https://raw.githubusercontent.com/oracle/oci-cli/master/scripts/install/install.sh)"
```

Edit `~/.oci/config`:
```
user=<YOUR USER OCID>
fingerprint=<KEY FINGERPRINT FROM PREVIOUS STEP>
key_file=~/.oci/oci_api_key.pem
tenancy=<YOUR TENANCY OCID>
region=<YOUR REGION e.g. eu-zurich-1>
```

Change the permission of the config file and check the console is working:
```
chmod 600 ~/.oci/config
oci iam user list
```


### Deploy a Kubernetes Cluster using Terraform

First, modify the variables from the `terraform.tfvars` file.

Then deploy the OKE cluster:
```
terraform init
terraform validate
terraform plan
terraform apply
```

The deployment should take around 5 minutes to complete + 2 more minutes for the node pool to be provisioned.

### Configure Kubectl
We can get the Kubectl configuration file directly from terraform:
```
terraform output oke_cluster_kube_config > ~/.kube/config
```

You can validate that everything works well with a command like:
```
kubectl get nodes
kubectl get all
```

# Install Oracle Service Broker
In this section we will follow the instructions from the OCI Service Broker Git repository:
https://github.com/oracle/oci-service-broker

The components that we want to create are the following:
![k8s_architecture.png](architecture/k8s_architecture.png)

### Install Helm
Both the Service Catalog and the OCI Service Broker are installed using Helm, so make sure Helm is installed. If it is not the case, just follow the official installation guide. We advise to use Helm v3 are it does not require the installation of the Tiller server.  

```
brew update && brew install helm
helm version
version.BuildInfo{Version:"v3.1.2", GitCommit:"d878d4d45863e42fd5cff6743294a11d28a9abce", GitTreeState:"clean", GoVersion:"go1.13.8"}
```

### Install the Service Catalog
```
helm repo add svc-cat https://svc-catalog-charts.storage.googleapis.com
helm install catalog svc-cat/catalog
```
To interact with the Service Catalog, also install the svcat tool:
```
brew update && brew install kubernetes-service-catalog-client
```

### Install the OCI Service Broker
The Service Broker needs your credentials to interact with the Oracle Cloud. We will create a secret for that. Edit the `kustomization.yml` file and create the secret:
```
cd k8s
cp /Users/goutaudi/.oci/oci_api_key.pem ./privatekey
kubectl apply -k .
export SECRET=$(kubectl get secrets -o custom-columns=NAME:.metadata.name | grep oci-credentials)
```

Check for the latest version of the Service Broker on https://github.com/oracle/oci-service-broker/releases/ and save the URL:
```
export CHART=https://github.com/oracle/oci-service-broker/releases/download/v1.4.0/oci-service-broker-1.4.0.tgz
```

Then install the service broker and register it:
```
helm install oci-service-broker $CHART --set ociCredentials.secretName=$SECRET --set storage.etcd.useEmbedded=true --set tls.enabled=false
kubectl create -f oci-service-broker.yml
```

### Prevision Autonomous Database
We can use svcat to check which services are available:
```
svcat get brokers
svcat get classes
svcat get plans
```

Now we can create a database. From a Kubernetes point of view, this database will just be a "Service Instance" from a specific "Class" (atp-service) with an associated billing plan (standard) and some configuration parameters. Edit the compartment id in the `atpdb.yml` file, and provision the database:
```
kubectl create -f atpdb.yml
svcat describe instance atpdb
```

The database instance is created as a PDB, so the provisioning process will only take 2 minutes.  


### Create a docker image for the client application
If not already done, connect to the OCI console and create an authentication token for your user (User Settings). We will use it to upload the image to the private repository.

```
cd docker/dbclient
docker build -t dbclient .
docker tag dbclient zrh.ocir.io/orasealps/osb/dbclient:7.7 
export OCI_USER='orasealps/oracleidentitycloudservice/guillaume.goutaudier@oracle.com'
export AUTH_TOKEN='REPLACE_WITH _YOUR_TOKEN'
docker login -u $OCI_USER -p $AUTH_TOKEN zrh.ocir.io
docker push zrh.ocir.io/orasealps/osb/dbclient:7.7  
```

We also need to create a kubernetes secret so we can later download the image:
```
export EMAIL='<YOUR_EMAIL>'
kubectl create secret docker-registry regcred --docker-server=zrh.ocir.io --docker-username=$OCI_USER --docker-password=$AUTH_TOKEN --docker-email=$EMAIL
```

### Connect to the database from a client application
Note that database instance was created with a "binding". When the DB was ready, this binding created a kubernetes secret with the DB wallet:
```
kubectl get secrets atpdb-binding -o json | jq '.'
```

This secret will be mounted to /etc/secret in the client application. We will now see how to decode the values contained in the secret and use them to the database from a Python application. We will run these commands interactively to illustrate what needs to be done, but they could be executed automatically at startup with an init container. 

```
kubectl create configmap dbclient-configmap --from-file=./app-configmap/
kubectl create -f app.yml
kubectl exec -it dbclient /bin/bash 
/bin/bash /scripts/decode_secrets.sh
```


We can now test the connection to the Oracle database:
```
python3 /scripts/test_connection.py <message you want to record>
```

# Clean-up
Delete configmap and client application:
```
kubectl delete -f app.yml
kubectl delete configmap/dbclient-configmap
```

Delete Database (synchronous):
```
kubectl delete -f atpdb.yml  
```

Alternatively (asynchronous):
```
svcat unbind atpdb
svcat deprovision atpdb
```

Delete all resources with Terraform:
```
terraform destroy
```
This will take around 3 minutes.

Alternatively, you can simply stop the VM instance(s) from your node pool and restart them when needed:
```
export COMPARTMENT_ID="ocid1.compartment.oc1..aaaaaaaafjqtlroamfbvixfelcr66rs4yf6xbhostwe44tgv7ehtatbpibfq"
export VM_ID=$(oci compute instance list --compartment-id=$COMPARTMENT_ID | jq -r '.data[0].id')
oci compute instance action --action=START --instance-id=$VM_ID
```
The 'ClusterServiceBroker' service might need to be restarted:
```
kubectl create -f oci-service-broker.yml
```

