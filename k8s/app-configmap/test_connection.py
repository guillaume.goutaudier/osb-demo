import sys, os, cx_Oracle

message = sys.argv[1]
os.environ['TNS_ADMIN'] = '/app'
con = cx_Oracle.connect(user = 'admin', password = 's123456789S@', dsn = 'atpdb_tp')
print("Successfully connected to Oracle database.")

cursor = con.cursor()
cursor.execute("CREATE TABLE MESSAGES ( ID NUMBER(32) , MSG VARCHAR2(32))")
statement = "INSERT INTO MESSAGES (ID, MSG) VALUES(1,'" + message + "')"
cursor.execute(statement)
cursor.execute("COMMIT")
print("Successfully created table and inserted value.")

cursor.execute("SELECT MSG FROM MESSAGES WHERE ID = 1")
print("Successfully retrieved value:")
print(cursor.fetchone()[0])
con.close()


