# ------ Create a Kubernetes cluster (OKE)
resource "oci_containerengine_cluster" "oke" {
    compartment_id = var.compartment_ocid
    # We do not use version 1.15.7 because of an issue with the Service Catalog
    kubernetes_version = "v1.14.8"
    name = "oke"
    vcn_id = oci_core_vcn.vcn.id
    options {
      service_lb_subnet_ids = [oci_core_subnet.public-subnet.id ]
    }
}
resource "oci_containerengine_node_pool" "pool" {
    cluster_id = oci_containerengine_cluster.oke.id
    compartment_id = var.compartment_ocid
    # We do not use version 1.15.7 because of an issue with the Service Catalog
    kubernetes_version = "v1.14.8"
    name = "pool"
    node_source_details {
        image_id = data.oci_core_images.images.images[0].id
        source_type = "IMAGE"
    }
    node_shape = "VM.Standard2.2"
    node_config_details {
        placement_configs {
            availability_domain = data.oci_identity_availability_domains.ads.availability_domains[0].name
            subnet_id = oci_core_subnet.private-subnet.id
        }
        size = "1"
    }
}

