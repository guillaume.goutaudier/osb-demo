# ------ Virtual Cloud Network
resource "oci_core_vcn" "vcn" {
  cidr_block = "10.5.0.0/16"
  compartment_id = var.compartment_ocid
  display_name = "oke-vcn"
}

# ------ Internet Gateway
resource "oci_core_internet_gateway" "ig" {
  compartment_id = var.compartment_ocid
  display_name = "oke-vcn-internet-gateway"
  vcn_id = oci_core_vcn.vcn.id
}

# ------ NAT Gateway
resource "oci_core_nat_gateway" "ng" {
  compartment_id = var.compartment_ocid
  vcn_id = oci_core_vcn.vcn.id
  display_name = "oke-vcn-nat-gateway"
}

# ------ Service Gateway
resource "oci_core_service_gateway" "sg" {
  compartment_id = var.compartment_ocid
  services {
    service_id = data.oci_core_services.paas_services.services.0.id
  }   
  vcn_id = oci_core_vcn.vcn.id
  display_name = "oke-vcn-service-gateway"
}

# ------ Route Table for the Public subnet
resource "oci_core_route_table" "public-rt" {
  compartment_id = var.compartment_ocid
  vcn_id = oci_core_vcn.vcn.id
  display_name = "oke-vcn-public-route-table"
  route_rules {
    destination = "0.0.0.0/0"
    network_entity_id = oci_core_internet_gateway.ig.id
  }
}

# ------ Security list for the Public Subnet
resource "oci_core_security_list" "public-sl" {
  compartment_id = var.compartment_ocid
  display_name = "oke-vcn-public-subnet-security-list"
  vcn_id = oci_core_vcn.vcn.id
  egress_security_rules {
    protocol = "all"
    destination = "0.0.0.0/0"
  }
  ingress_security_rules {
    protocol = "all"
    source = "0.0.0.0/0"
  }
}

# ------ Public subnet in AD1 in the new VCN
resource "oci_core_subnet" "public-subnet" {
  availability_domain = data.oci_identity_availability_domains.ads.availability_domains[0].name
  cidr_block = "10.5.1.0/24"
  display_name = "oke-vcn-public-subnet"
  compartment_id = var.compartment_ocid
  vcn_id = oci_core_vcn.vcn.id
  route_table_id = oci_core_route_table.public-rt.id
  security_list_ids = [oci_core_security_list.public-sl.id]
  dhcp_options_id = oci_core_vcn.vcn.default_dhcp_options_id
}

# ------ Route Table for the Private subnet
resource "oci_core_route_table" "private-rt" {
  compartment_id = var.compartment_ocid
  vcn_id = oci_core_vcn.vcn.id
  display_name = "oke-vcn-private-route-table"
  route_rules {
    destination = "0.0.0.0/0"
    network_entity_id = oci_core_nat_gateway.ng.id
  }
  route_rules {
    destination_type = "SERVICE_CIDR_BLOCK"
    destination = data.oci_core_services.paas_services.services[0].cidr_block
    network_entity_id = oci_core_service_gateway.sg.id
  }
}

# ------ Security list for the Private Subnet
resource "oci_core_security_list" "private-sl" {
  compartment_id = var.compartment_ocid
  display_name = "oke-vcn-private-subnet-security-list"
  vcn_id = oci_core_vcn.vcn.id
  egress_security_rules {
    protocol = "all"
    destination = "0.0.0.0/0"
  }
  ingress_security_rules {
    protocol = "all"
    source = "0.0.0.0/0"
  }
}

# ------ Private subnet in AD1 in the new VCN
resource "oci_core_subnet" "private-subnet" {
  availability_domain = data.oci_identity_availability_domains.ads.availability_domains[0].name
  cidr_block = "10.5.2.0/24"
  display_name = "oke-vcn-private-subnet"
  compartment_id = var.compartment_ocid
  vcn_id = oci_core_vcn.vcn.id
  route_table_id = oci_core_route_table.private-rt.id
  security_list_ids = [oci_core_security_list.private-sl.id]
  dhcp_options_id = oci_core_vcn.vcn.default_dhcp_options_id
}

