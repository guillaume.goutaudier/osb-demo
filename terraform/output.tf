# ------ Get availability domains
data "oci_identity_availability_domains" "ads" {
  compartment_id = var.compartment_ocid
}
output "node_pool_availability_domain" {
  value = data.oci_identity_availability_domains.ads.availability_domains[0].name
}

# ------ Get latest image
data "oci_core_images" "images" {
  compartment_id = var.compartment_ocid
  operating_system = "Oracle Linux"
  operating_system_version = "7.7"
  shape = "VM.Standard2.2"
  sort_by = "TIMECREATED"
}
output "latest_image_to_be_deployed" {
  value = data.oci_core_images.images.images[0].id
}

# ------ Get list of PaaS services
data "oci_core_services" "paas_services" {
}
output "services_used_for_service_gateway" {
  value = data.oci_core_services.paas_services.services[0]
}

# ------ Get kubectl config from OKE cluster
data "oci_containerengine_cluster_kube_config" "oke_cluster_kube_config" {
    cluster_id = oci_containerengine_cluster.oke.id
}
output "oke_cluster_kube_config" {
  value = data.oci_containerengine_cluster_kube_config.oke_cluster_kube_config.content
}

